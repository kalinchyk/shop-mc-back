drop index if exists currency_id;
drop index if exists color_id;
drop index if exists action_id;
drop index if exists product_id;
drop index if exists product_history_id;
drop index if exists product_history_product_id;
drop index if exists price_id;
drop index if exists price_history_id;
drop index if exists donate_session;
drop index if exists donate_product_history_id;
drop index if exists donate_status;
drop index if exists donate_orderid;
drop index if exists donate_history_id;

drop table if exists currency;
drop table if exists status;
drop table if exists color;
drop table if exists action;
drop table if exists price;
drop table if exists price_history;
drop table if exists product;
drop table if exists product_history;
drop table if exists donate;
drop table if exists donate_history;

create table currency (
	id integer primary key,
	code text,
	name text
);
create index currency_id on currency(id);
insert into currency (id, code, name) values
	(980, 'uah', 'грн.'),
	(643, 'rub', 'руб.')
;

create table color (
	id integer primary key autoincrement, 
	code text,
	name text
);
create index color_id on color(id);
insert into color (code, name) values 
	('red', 'красный'), 
	('green', 'зеленый'), 
	('blue', 'синий')
;

create table action (
	id integer primary key autoincrement,
	code text,
	name text
);
create index action_id on action(id);
insert into action (code, name) values
	('created', 'создано'),
	('updated', 'изменено'),
	('deleted', 'удалено')
;

create table product (
	id integer primary key autoincrement,
	name text, 
	color_id integer references color (id), 
	description text, 
	detail text, 
	amount real,
	command text
);
create index product_id on product(id);
create table product_history (
	id integer primary key autoincrement,
	date timestamp,
	action_id integer references action (id),

	product_id integer references product (id),
	name text, 
	color_id integer references color (id), 
	description text, 
	detail text, 
	amount real,
	command text
);
create index product_history_id on product_history(id);
create index product_history_product_id on product_history(product_id);

create table price (
	id integer primary key autoincrement,
	amount real,
	currency_id integer references currency (id),
	product_id integer references product (id)
);
create index price_id on price(id);
create table price_history (
	id integer primary key autoincrement,
	date timestamp,
	action_id integer references action (id),

	amount real,
	currency_id integer references currency (id),
	product_history_id integer references product_history (id)
);
create index price_history_id on price_history(id);

create table donate (
	id integer primary key autoincrement, 
	user_login text, 
	product_history_id integer references product_history (id), 
	status text,
	ipaddr text,
	useragent text,
	session text,
	orderid text
);
create index donate_session on donate(session);
create index donate_product_history_id on donate(product_history_id);
create index donate_status on donate(status);
create index donate_orderid on donate(orderid);
create table donate_history (
	id integer primary key autoincrement, 
	date timestamp,
	action_id integer references action (id),

	donate_id integer references donate (id),
	status text,
	ipaddr text,
	request text
);
create index donate_history_id on donate_history(id);

