import re
import glob
import time
import json
import sqlite3
from shutil import copyfile

config = json.load(
    open(
        './minecraft.json'
    )
)

con = sqlite3.connect(
    config['sqlite']['db'],
    detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
)
cur = con.cursor()


def get_tables():
    cur.execute(
        'select name '
        'from sqlite_master '
        'where type=\'table\''
    )
    return cur.fetchall()


def get_version():
    ver = 0
    for item in cur.execute(
            'select version '
            'from version;'
    ):
        (ver,) = item or (0,)
    return ver


version = 0
if ("version",) in get_tables():
    version = get_version()
else:
    cur.execute(
        'create table version '
        '(version integer);'
    )
    cur.execute(
        'insert into version '
        'values (\'0\');'
    )

for item in sorted(
        glob.glob(
            "./db_*.sql"
        )
):
    temp = int(re.search("db_(\d+)\.sql", item).group(1))
    if temp > version:
        copyfile(
            config['sqlite']['db'],
            "~%s" % time.time()
        )
        cur.executescript(
            open(item).read()
        )
        cur.execute(
            'update version '
            'set version = ?;',
            (
                temp,
            )
        )
        version = get_version()
    con.commit()

print version
