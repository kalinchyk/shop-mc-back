# -*- coding: utf-8 -*-

import os
import sys
import json
from bottle import response
from bottle import default_app, hook
from bottle_sqlite import SQLitePlugin
from bottle_errorsrest import ErrorsRestPlugin

os.chdir(os.path.dirname(__file__))
sys.path.insert(0, os.path.dirname(__file__))

import api_public
import api_private
import api_process

api_process.app = api_private.app = api_public.app = app = application = default_app()

config = {
    "autojson": True,
    "minecraft": {
        "acquiring": {
            "form_method": "post",
            "form_url": "https://sci.interkassa.com/",
            "id": "",
            "sign": "",
            "sign_test": "",
            "type": "interkassa"
        },
        "admin_username": "root",
        "admin_password": "toor",
        "cookie_name": "minecraft",
        "cookie_sign": "minecraft",
        "server_host": "127.0.0.1",
        "server_query_port": 25565,
        "server_rcon_password": "",
        "server_rcon_port": 25575,
        "server_url": "http://localhost:4200",
        "server_api": "https://test.kalinchyk.com/mc"
    },
    "response": {
        "allow_headers": "Authorization, Origin, Accept, Content-Type, X-Requested-With",
        "allow_methods": "PUT, GET, POST, DELETE, OPTIONS",
        "allow_origin": "*",
        "cache_control": "no-cache",
        "content_type": "application/json; charset=utf-8"
    },
    "sqlite": {
        "db": "minecraft.db"
    }
}

with open('minecraft.json') as fp:
    config.update(
        json.load(
            fp
        )
    )

app.config.load_dict(
    config
)

app.install(
    SQLitePlugin(
        dbfile=app.config['sqlite.db']
    )
)
app.install(
    ErrorsRestPlugin()
)


@hook('before_request')
def _reload():
    reload(api_public)
    reload(api_private)
    reload(api_process)
    api_process.app = api_private.app = api_public.app = app


@hook('after_request')
def _security():
    response.headers['Access-Control-Allow-Origin'] = app.config['response.allow_origin']
    response.headers['Access-Control-Allow-Methods'] = app.config['response.allow_methods']
    response.headers['Access-Control-Allow-Headers'] = app.config['response.allow_headers']

    response.headers['Content-Type'] = app.config['response.content_type']
    response.headers['Cache-Control'] = app.config['response.cache_control']
